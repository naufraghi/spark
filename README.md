# Spark ECS engine

## Requirements

	* `GLib` development files
	* A C99-compatible compiler (CLang / GCC works fine)

## Building

	`make`

## Tests

	`make tests`
