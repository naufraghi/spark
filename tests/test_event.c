#include "game.h"
#include "mapped_pool.h"
#include <glib.h>
#include <stdbool.h>

#define COMPONENT_DUMMY GAME_CORE + 1
#define EVENT_FOO 1
#define EVENT_BAR 2

#define SET_BIT(val, bitIndex) val |= (1 << bitIndex)
#define CLEAR_BIT(val, bitIndex) val &= ~(1 << bitIndex)
#define TOGGLE_BIT(val, bitIndex) val ^= (1 << bitIndex)
#define BIT_IS_SET(val, bitIndex) (val & (1 << bitIndex))

struct DummySystem {
	MappedPool *components;
};

struct DummyComponent {
	Entity e;
};

struct EntityLevelHandlerTrace {
	Entity entity;
	EventType event;
	bool notified;
	bool error;
};

static void
sys_init(struct System *self)
{
	struct DummySystem *sys = g_new0(struct DummySystem, 1);
	sys->components = mapped_pool_new(
		sizeof(struct DummyComponent), 5, 5
	);

	self->data = sys;
}

static void
sys_exit(struct System *self)
{
	struct DummySystem *sys = self->data;
	mapped_pool_free(sys->components);
	g_free(sys);
}

static void
sys_create_component(struct System *self, Entity entity)
{
	struct DummySystem *sys = self->data;
	struct DummyComponent c = {
		.e = entity
	};
	mapped_pool_set(sys->components, entity, &c);
}

static void
sys_destroy_component(struct System *self, Entity entity)
{
	struct DummySystem *sys = self->data;
	mapped_pool_pop(sys->components, entity);
}

static void
sys_update(struct System *self)
{
	struct DummySystem *sys = self->data;
	Pool *p = mapped_pool_storage_get(sys->components);
	PoolIter i;

	pool_iter_init(p, &i);
	while (pool_iter_next(&i)) {
		struct DummyComponent *c = pool_iter_get(&i);
		if (c->e != 4) {
			game_event_push(
				COMPONENT_DUMMY, EVENT_FOO, c->e, NULL, 0
			);
		}

		if (c->e != 5) {
			game_event_push(
				COMPONENT_DUMMY, EVENT_BAR, c->e, NULL, 0
			);
		}
	}
}

static void
setup(gpointer fixture, gconstpointer user_data)
{
	game_init();

	struct System *sys = g_new0(struct System, 1);
	sys->name = "dummy";
	sys->init = sys_init;
	sys->exit = sys_exit;
	sys->update = sys_update;
	sys->create_component = sys_create_component;
	sys->destroy_component = sys_destroy_component;

	game_system_register(COMPONENT_DUMMY, sys);
}

static void
teardown(gpointer fixture, gconstpointer user_data)
{
	game_system_unregister(COMPONENT_DUMMY);
	game_exit();
}

static void
init_and_deinit_handler(struct Event *evt, void *data)
{
	g_assert_true(evt->component == GAME_CORE);

	unsigned char *flags = data;
	if (evt->event == EVENT_ENTITY_CREATED)
		SET_BIT(*flags, 0);
	else if (evt->event == EVENT_ENTITY_DESTROYED)
		SET_BIT(*flags, 1);
}

/**
 * Test some of game events, not tied to any system.
 */
static void
test_entity_init_and_deinit_subscription(
	gpointer fixture, gconstpointer user_data
) {
	unsigned char flags = 0;
	game_event_subscribe(
		GAME_CORE,               // core engine event
		EVENT_ENTITY_CREATED,    // notify on entity creation
		ENTITY_ANY,              // no specific entity
		init_and_deinit_handler,
		&flags
	);

	game_event_subscribe(
		GAME_CORE,               // core engine event
		EVENT_ENTITY_DESTROYED,  // notify on entity destruction
		ENTITY_ANY,              // no specific entity
		init_and_deinit_handler,
		&flags
	);

	Entity e;

	e = game_entity_create(NULL);
	game_update();
	g_assert_true(BIT_IS_SET(flags, 0));

	game_entity_destroy(e);
	game_update();
	g_assert_true(BIT_IS_SET(flags, 1));
}

static void
entity_level_handler(struct Event *evt, void *data)
{
	struct EntityLevelHandlerTrace *t = data;

	if (evt->component != COMPONENT_DUMMY ||
	    evt->event != t->event ||
	    evt->entity != t->entity) {
		t->error = true;
	}
	else {
		t->notified = true;
	}
}

/**
 * Test specific subscriptions at entity level.
 */
static void
test_entity_level_subscription(gpointer fixture, gconstpointer user_data)
{
	ComponentType components[] = {
		COMPONENT_DUMMY, 0
	};

	Entity e1 = game_entity_create(components);
	Entity e2 = game_entity_create(components);
	Entity e3 = game_entity_create(components);
	Entity e4 = game_entity_create(components);

	struct EntityLevelHandlerTrace t1 = {
		.entity = e1,
		.event = EVENT_FOO,
		.notified = false,
		.error = false
	};

	struct EntityLevelHandlerTrace t2 = {
		.entity = e2,
		.event = EVENT_FOO,
		.notified = false,
		.error = false
	};

	struct EntityLevelHandlerTrace t3_foo = {
		.entity = e3,
		.event = EVENT_FOO,
		.notified = false,
		.error = false
	};

	struct EntityLevelHandlerTrace t3_bar = {
		.entity = e3,
		.event = EVENT_BAR,
		.notified = false,
		.error = false
	};

	struct EntityLevelHandlerTrace t4_foo = {
		.entity = e4,
		.event = EVENT_FOO,
		.notified = false,
		.error = false
	};

	struct EntityLevelHandlerTrace t4_bar = {
		.entity = e4,
		.event = EVENT_BAR,
		.notified = false,
		.error = false
	};

	game_event_subscribe(
		COMPONENT_DUMMY,
		EVENT_FOO,
		e1,
		entity_level_handler,
		&t1
	);

	game_event_subscribe(
		COMPONENT_DUMMY,
		EVENT_FOO,
		e2,
		entity_level_handler,
		&t2
	);

	game_event_subscribe(
		COMPONENT_DUMMY,
		EVENT_FOO,
		e3,
		entity_level_handler,
		&t3_foo
	);

	game_event_subscribe(
		COMPONENT_DUMMY,
		EVENT_BAR,
		e3,
		entity_level_handler,
		&t3_bar
	);

	game_event_subscribe(
		COMPONENT_DUMMY,
		EVENT_FOO,
		e4,
		entity_level_handler,
		&t4_foo
	);

	game_event_subscribe(
		COMPONENT_DUMMY,
		EVENT_BAR,
		e4,
		entity_level_handler,
		&t4_bar
	);

	game_update();

	g_assert_true(t1.notified);
	g_assert_false(t1.error);

	g_assert_true(t2.notified);
	g_assert_false(t2.error);

	g_assert_false(t3_foo.notified);
	g_assert_false(t3_foo.error);

	g_assert_true(t3_bar.notified);
	g_assert_false(t3_bar.error);

	g_assert_true(t4_foo.notified);
	g_assert_false(t4_foo.error);

	g_assert_false(t4_bar.notified);
	g_assert_false(t4_bar.error);
}

/**
 * Test that event handlers bound to specific entities aren't called on zombies.
 */
static void
test_event_absence_for_dead_entities(gpointer fixture, gconstpointer user_data)
{
	ComponentType components[] = {
		COMPONENT_DUMMY, 0
	};

	Entity e = game_entity_create(components);

	struct EntityLevelHandlerTrace t = {
		.entity = e,
		.event = EVENT_FOO,
		.notified = false,
		.error = false
	};

	game_event_subscribe(
		COMPONENT_DUMMY,
		EVENT_FOO,
		e,
		entity_level_handler,
		&t
	);

	game_update();
	g_assert_true(t.notified);
	g_assert_false(t.error);

	game_entity_destroy(e);
	t.notified = t.error = false;

	game_update();
	g_assert_false(t.notified);
	g_assert_false(t.error);
}

/**
 * Test that after destroying an entity, subscribers are removed and after
 * having created a new entity again, they're not called.
 */
static void
test_subscribers_removal_for_dead_entities(
	gpointer fixture, gconstpointer user_data
) {
	ComponentType components[] = {
		COMPONENT_DUMMY, 0
	};

	Entity e = game_entity_create(components);

	struct EntityLevelHandlerTrace t_foo = {
		.entity = e,
		.event = EVENT_FOO,
		.notified = false,
		.error = false
	};

	struct EntityLevelHandlerTrace t_bar = {
		.entity = e,
		.event = EVENT_BAR,
		.notified = false,
		.error = false
	};

	game_event_subscribe(
		COMPONENT_DUMMY,
		EVENT_FOO,
		e,
		entity_level_handler,
		&t_foo
	);

	game_event_subscribe(
		COMPONENT_DUMMY,
		EVENT_BAR,
		e,
		entity_level_handler,
		&t_bar
	);

	game_update();

	g_assert_true(t_foo.notified);
	g_assert_false(t_foo.error);

	g_assert_true(t_bar.notified);
	g_assert_false(t_bar.error);

	t_foo.notified = t_bar.notified = false;
	game_entity_destroy(e);
	game_update();

	g_assert_false(t_foo.notified);
	g_assert_false(t_foo.error);

	g_assert_false(t_bar.notified);
	g_assert_false(t_bar.error);
}

static void
event_level_handler(struct Event *evt, void *data)
{
	unsigned char *flags = data;

	if (evt->component != COMPONENT_DUMMY && evt->event != EVENT_FOO) {
		SET_BIT(*flags, 3);
		return;
	}

	if (evt->entity == 2)
		SET_BIT(*flags, 0);
	else if (evt->entity == 3)
		SET_BIT(*flags, 1);
}

/**
 * Test a subscription which catches all events fired by given component and
 * specific event, not taking care of which entity it belongs to.
 */
static void
test_event_level_subscription(gpointer fixture, gconstpointer user_data)
{
	unsigned char flags = 0;
	game_event_subscribe(
		COMPONENT_DUMMY,
		EVENT_FOO,
		0,
		event_level_handler,
		&flags
	);

	ComponentType components[] = {
		COMPONENT_DUMMY, 0
	};

	Entity e1 = game_entity_create(components);
	Entity e2 = game_entity_create(components);

	game_update();

	g_assert_true(BIT_IS_SET(flags, 0));
	g_assert_true(BIT_IS_SET(flags, 1));
	g_assert_false(BIT_IS_SET(flags, 2));
}

static void
component_level_handler(struct Event *evt, void *data)
{
	unsigned char *flags = data;

	if (evt->component != COMPONENT_DUMMY) {
		SET_BIT(*flags, 2);
		return;
	}

	if (evt->event == EVENT_FOO)
		SET_BIT(*flags, 0);
	else if (evt->event == EVENT_BAR)
		SET_BIT(*flags, 1);
}

/**
 * Test a subscription which catches all component-level events.
 */
static void
test_component_level_subscription(gpointer fixture, gconstpointer user_data)
{
	unsigned char flags = 0;
	game_event_subscribe(
		COMPONENT_DUMMY,
		0,
		0,
		component_level_handler,
		&flags
	);

	ComponentType components[] = {
		COMPONENT_DUMMY, 0
	};

	Entity e1 = game_entity_create(components);
	Entity e2 = game_entity_create(components);

	game_update();

	g_assert_true(BIT_IS_SET(flags, 0));
	g_assert_true(BIT_IS_SET(flags, 1));
	g_assert_false(BIT_IS_SET(flags, 2));
}

static void
handler1(struct Event *evt, void *data)
{
	struct EntityLevelHandlerTrace *t = data;
	t->notified = evt->event == t->event && evt->entity == t->entity;
}

static void
handler2(struct Event *evt, void *data)
{
	struct EntityLevelHandlerTrace *t = data;
	t->notified = evt->event == t->event && evt->entity == t->entity;
}

/**
 * Test selective callback unsubscription.
 */
static void
test_unsubscribe(gpointer fixture, gconstpointer user_data)
{
	ComponentType components[] = {
		COMPONENT_DUMMY, 0
	};

	Entity e = game_entity_create(components);

	struct EntityLevelHandlerTrace t_foo1 = {
		.entity = e,
		.event = EVENT_FOO,
		.notified = false,
		.error = false
	};
	struct EntityLevelHandlerTrace t_foo2 = t_foo1;

	struct EntityLevelHandlerTrace t_bar1 = {
		.entity = e,
		.event = EVENT_BAR,
		.notified = false,
		.error = false
	};
	struct EntityLevelHandlerTrace t_bar2 = t_bar1;

	game_event_subscribe(
		COMPONENT_DUMMY,
		EVENT_FOO,
		e,
		handler1,
		&t_foo1
	);

	game_event_subscribe(
		COMPONENT_DUMMY,
		EVENT_FOO,
		e,
		handler2,
		&t_foo2
	);

	game_event_subscribe(
		COMPONENT_DUMMY,
		EVENT_BAR,
		e,
		handler1,
		&t_bar1
	);

	game_event_subscribe(
		COMPONENT_DUMMY,
		EVENT_BAR,
		e,
		handler2,
		&t_bar2
	);

	game_update();

	g_assert_true(t_foo1.notified);
	g_assert_true(t_foo2.notified);
	g_assert_true(t_bar1.notified);
	g_assert_true(t_bar2.notified);

	game_event_unsubscribe(COMPONENT_DUMMY, EVENT_FOO, e, handler1);
	game_event_unsubscribe(COMPONENT_DUMMY, EVENT_BAR, e, handler2);

	t_foo1.notified = t_foo2.notified = false;
	t_bar1.notified = t_bar2.notified = false;

	game_update();

	g_assert_false(t_foo1.notified);
	g_assert_true(t_foo2.notified);
	g_assert_true(t_bar1.notified);
	g_assert_false(t_bar2.notified);
}

/**
 * Test unsubscription of all callbacks for given event signature.
 */
static void
test_unsubscribe_all(gpointer fixture, gconstpointer user_data)
{
	ComponentType components[] = {
		COMPONENT_DUMMY, 0
	};

	Entity e = game_entity_create(components);

	struct EntityLevelHandlerTrace t_foo1 = {
		.entity = e,
		.event = EVENT_FOO,
		.notified = false,
		.error = false
	};
	struct EntityLevelHandlerTrace t_foo2 = t_foo1;

	struct EntityLevelHandlerTrace t_bar1 = {
		.entity = e,
		.event = EVENT_BAR,
		.notified = false,
		.error = false
	};
	struct EntityLevelHandlerTrace t_bar2 = t_bar1;

	game_event_subscribe(
		COMPONENT_DUMMY,
		EVENT_FOO,
		e,
		entity_level_handler,
		&t_foo1
	);

	game_event_subscribe(
		COMPONENT_DUMMY,
		EVENT_FOO,
		e,
		entity_level_handler,
		&t_foo2
	);

	game_event_subscribe(
		COMPONENT_DUMMY,
		EVENT_BAR,
		e,
		entity_level_handler,
		&t_bar1
	);

	game_event_subscribe(
		COMPONENT_DUMMY,
		EVENT_BAR,
		e,
		entity_level_handler,
		&t_bar2
	);

	game_update();

	g_assert_true(t_foo1.notified);
	g_assert_true(t_foo2.notified);
	g_assert_true(t_bar1.notified);
	g_assert_true(t_bar2.notified);

	game_event_unsubscribe_all(COMPONENT_DUMMY, EVENT_FOO, e);
	game_event_unsubscribe_all(COMPONENT_DUMMY, EVENT_BAR, e);

	t_foo1.notified = t_foo2.notified = false;
	t_bar1.notified = t_bar2.notified = false;

	game_update();

	g_assert_false(t_foo1.notified);
	g_assert_false(t_foo2.notified);
	g_assert_false(t_bar1.notified);
	g_assert_false(t_bar2.notified);
}


static void
test_unsubscribe_on_component_removal(gpointer fixture, gconstpointer user_data)
{
	ComponentType components[] = {
		COMPONENT_DUMMY, 0
	};

	Entity e = game_entity_create(components);

	struct EntityLevelHandlerTrace t = {
		.entity = e,
		.event = EVENT_FOO,
		.notified = false,
		.error = false
	};

	game_event_subscribe(
		COMPONENT_DUMMY,
		EVENT_FOO,
		e,
		entity_level_handler,
		&t
	);

	game_update();
	g_assert_true(t.notified);

	t.notified = false;
	game_entity_component_remove(e, COMPONENT_DUMMY);

	game_update();
	g_assert_false(t.notified);
}

void
event_suite_setup()
{
	g_test_add(
		"/event/entity_init_and_deinit_subscription",
		void,
		NULL,
		setup,
		test_entity_init_and_deinit_subscription,
		teardown
	);

	g_test_add(
		"/event/entity_level_subscription",
		void,
		NULL,
		setup,
		test_entity_level_subscription,
		teardown
	);

	g_test_add(
		"/event/event_absence_for_dead_entities",
		void,
		NULL,
		setup,
		test_event_absence_for_dead_entities,
		teardown
	);

	g_test_add(
		"/event/subscribers_removal_for_dead_entities",
		void,
		NULL,
		setup,
		test_subscribers_removal_for_dead_entities,
		teardown
	);

	g_test_add(
		"/event/unsubscribe_on_component_removal",
		void,
		NULL,
		setup,
		test_unsubscribe_on_component_removal,
		teardown
	);

	g_test_add(
		"/event/event_level_subscription",
		void,
		NULL,
		setup,
		test_event_level_subscription,
		teardown
	);

	g_test_add(
		"/event/component_level_subscription",
		void,
		NULL,
		setup,
		test_component_level_subscription,
		teardown
	);

	g_test_add(
		"/event/unsubscribe",
		void,
		NULL,
		setup,
		test_unsubscribe,
		teardown
	);

	g_test_add(
		"/event/unsubscribe_all",
		void,
		NULL,
		setup,
		test_unsubscribe_all,
		teardown
	);

}
