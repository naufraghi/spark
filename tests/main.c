#include <glib.h>

extern void event_suite_setup();
extern void mapped_pool_suite_setup();
extern void pool_suite_setup();
extern void system_suite_setup();

int
main(int argc, char **argv)
{
	g_test_init(&argc, &argv, NULL);

	event_suite_setup();
	mapped_pool_suite_setup();
	pool_suite_setup();
	system_suite_setup();

	g_test_run();
}
