#include "game.h"
#include "mapped_pool.h"
#include <glib.h>
#include <stdbool.h>
#include <stdio.h>

#define DUMMY_COMPONENT 2

struct {
	bool init_called;
	bool exit_called;
} checklist;

struct DummySystem{
	MappedPool *components;
	int counter;
};

struct DummyComponent {
	int number;
	bool updated;
};

static void
sys_init(struct System *self)
{
	struct DummySystem *sys = g_new(struct DummySystem, 1);
	sys->components = mapped_pool_new(sizeof(struct DummyComponent), 5, 5);
	sys->counter = 0;
	self->data = sys;

	checklist.init_called = true;
}

static void
sys_exit(struct System *self)
{
	struct DummySystem *sys = self->data;
	mapped_pool_free(sys->components);
	g_free(sys);

	checklist.exit_called = true;
}

static void
sys_update(struct System *self)
{
	struct DummySystem *sys = game_system_get(DUMMY_COMPONENT)->data;
	Pool *p = mapped_pool_storage_get(sys->components);
	PoolIter i;
	pool_iter_init(p, &i);
	while (pool_iter_next(&i)) {
		struct DummyComponent *c = pool_iter_get(&i);
		c->updated = true;
	}

}

static void
sys_create_component(struct System *self, Entity entity)
{
	struct DummySystem *sys = self->data;
	struct DummyComponent c = {
		.number = ++(sys->counter),
		.updated = false
	};
	mapped_pool_set(sys->components, entity, &c);
}

static void
sys_destroy_component(struct System *self, Entity entity)
{
	struct DummySystem *sys = self->data;
	mapped_pool_pop(sys->components, entity);
}

static void*
sys_get_component(struct System *self, Entity entity)
{
	struct DummySystem *sys = self->data;
	return mapped_pool_get(sys->components, entity);
}

static void
setup(gpointer fixture, gconstpointer user_data)
{
	struct System *sys = g_new0(struct System, 1);
	sys->name = "dummy";
	sys->init = sys_init;
	sys->exit = sys_exit;
	sys->update = sys_update;
	sys->create_component = sys_create_component;
	sys->destroy_component = sys_destroy_component;
	sys->get_component = sys_get_component;

	game_init();
	game_system_register(DUMMY_COMPONENT, sys);
}

static void
teardown(gpointer fixture, gconstpointer user_data)
{
	game_exit();
}

static void
test_registration(gpointer fixture, gconstpointer user_data)
{
	g_assert_true(checklist.init_called);
	game_system_unregister(DUMMY_COMPONENT);
	g_assert_true(checklist.exit_called);
}

static void
test_entity_creation(gpointer fixture, gconstpointer user_data)
{
	for (int i = 1; i <= 10; i++) {
		const ComponentType components[] = { DUMMY_COMPONENT, 0 };
		Entity e = game_entity_create(components);

		struct DummyComponent *c = game_entity_component_get(
			e, DUMMY_COMPONENT
		);
		g_assert_cmpuint(c->number, ==, i);
	}
}


static void
test_entity_destruction(gpointer fixture, gconstpointer user_data)
{
	struct DummySystem *sys = game_system_get(DUMMY_COMPONENT)->data;
	const ComponentType components[] = { DUMMY_COMPONENT, 0 };
	Entity e1 = game_entity_create(components);
	Entity e2 = game_entity_create(components);
	Entity e3 = game_entity_create(components);

	game_entity_destroy(e2);
	g_assert_cmpuint(
		pool_len(mapped_pool_storage_get(sys->components)),
		==,
		2
	);

	game_entity_destroy(e1);
	g_assert_cmpuint(
		pool_len(mapped_pool_storage_get(sys->components)),
		==,
		1
	);

	game_entity_destroy(e3);
	g_assert_cmpuint(
		pool_len(mapped_pool_storage_get(sys->components)),
		==,
		0
	);
}

static void
test_component_add_and_remove(gpointer fixture, gconstpointer user_data)
{
	struct DummySystem *sys = game_system_get(DUMMY_COMPONENT)->data;
	Entity e = game_entity_create(NULL);

	g_assert_cmpuint(
		pool_len(mapped_pool_storage_get(sys->components)), ==, 0
	);

	game_entity_component_add(e, DUMMY_COMPONENT);
	g_assert_cmpuint(
		pool_len(mapped_pool_storage_get(sys->components)), ==, 1
	);

	game_entity_component_remove(e, DUMMY_COMPONENT);
	g_assert_cmpuint(
		pool_len(mapped_pool_storage_get(sys->components)), ==, 0
	);
}

static void
test_component_existence_check(gpointer fixture, gconstpointer user_data)
{
	const ComponentType components[] = { DUMMY_COMPONENT, 0 };
	Entity e = game_entity_create(components);

	g_assert_true(game_entity_has_component(e, DUMMY_COMPONENT));

	game_entity_component_remove(e, DUMMY_COMPONENT);
	g_assert_false(game_entity_has_component(e, DUMMY_COMPONENT));
}

static void
test_update(gpointer fixture, gconstpointer user_data)
{
	const ComponentType components[] = { DUMMY_COMPONENT, 0 };
	Entity e1 = game_entity_create(components);
	Entity e2 = game_entity_create(components);
	Entity e3 = game_entity_create(components);

	game_update();

	struct DummyComponent *c1 = game_entity_component_get(
		e1, DUMMY_COMPONENT
	);
	g_assert_true(c1->updated);

	struct DummyComponent *c2 = game_entity_component_get(
		e2, DUMMY_COMPONENT
	);
	g_assert_true(c1->updated);

	struct DummyComponent *c3 = game_entity_component_get(
		e3, DUMMY_COMPONENT
	);
	g_assert_true(c1->updated);
}

void
system_suite_setup()
{
	g_test_add(
		"/system/registration",
		void,
		NULL,
		setup,
		test_registration,
		teardown
	);

	g_test_add(
		"/system/entity_creation",
		void,
		NULL,
		setup,
		test_entity_creation,
		teardown
	);

	g_test_add(
		"/system/entity_destruction",
		void,
		NULL,
		setup,
		test_entity_destruction,
		teardown
	);

	g_test_add(
		"/system/component_add_and_remove",
		void,
		NULL,
		setup,
		test_component_add_and_remove,
		teardown
	);

	g_test_add(
		"/system/component_existence_check",
		void,
		NULL,
		setup,
		test_component_existence_check,
		teardown
	);

	g_test_add(
		"/system/update",
		void,
		NULL,
		setup,
		test_update,
		teardown
	);
}
