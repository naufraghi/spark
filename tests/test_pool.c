#include "pool.h"
#include <glib.h>
#include <string.h>

static void
setup(Pool **p, gconstpointer user_data)
{
	*p = pool_new(1, 10, 5);
}

static void
teardown(Pool **p, gconstpointer user_data)
{
	pool_free(*p);
}

static void
test_create(Pool **p, gconstpointer user_data)
{
	g_assert_cmpuint(pool_len(*p), ==, 0);
	g_assert_cmpuint(pool_size(*p), ==, 10);
	g_assert_cmpuint(pool_grow_size(*p), ==, 5);
}

static void
test_reserve(Pool **p, gconstpointer user_data)
{
	// reserve the first 10 elements, which are pre-allocated
	for (int i = 0; i < 10; i++) {
		int pos = pool_reserve(*p);
		g_assert_cmpint(i, ==, pos);
	}

	// space in pool must be finished
	g_assert_cmpuint(pool_len(*p), ==, 10);
	g_assert_cmpuint(pool_size(*p), ==, 10);


	// reserve another element, which must make the pool grow by 5 elements
	int pos = pool_reserve(*p);
	g_assert_cmpint(pos, ==, 10);

	// check new length and size
	g_assert_cmpuint(pool_len(*p), ==, 11);
	g_assert_cmpuint(pool_size(*p), ==, 15);
}

static void
test_use(Pool **p, gconstpointer user_data)
{
	char a = 'a', b = 'b', c = 'c', d = 'd', e = 'e';
	int pos;

	pos = pool_add(*p, &a);
	g_assert_cmpint(pos, ==, 0);
	g_assert_cmpuint(*((char*)pool_get(*p, pos)), ==, 'a');

	pos = pool_add(*p, &b);
	g_assert_cmpint(pos, ==, 1);
	g_assert_cmpuint(*((char*)pool_get(*p, pos)), ==, 'b');

	pos = pool_add(*p, &c);
	g_assert_cmpint(pos, ==, 2);
	g_assert_cmpuint(*((char*)pool_get(*p, pos)), ==, 'c');

	pool_set(*p, 4, &d);
	g_assert_cmpuint(*((char*)pool_get(*p, 4)), ==, 'd');

	pos = pool_add(*p, &e);
	g_assert_cmpint(pos, ==, 3);
	g_assert_cmpuint(*((char*)pool_get(*p, pos)), ==, 'e');

	g_assert_cmpuint(pool_len(*p), ==, 5);
	g_assert_cmpuint(pool_size(*p), ==, 10);
}

static void
test_remove(Pool **p, gconstpointer user_data)
{
	char a = 'a', b = 'b', c = 'c';
	pool_set(*p, 5, &a);
	pool_add(*p, &b);
	pool_add(*p, &c);

	g_assert_cmpuint(pool_len(*p), ==, 3);
	g_assert_cmpuint(pool_size(*p), ==, 10);

	pool_remove(*p, 5);
	g_assert_cmpuint(pool_len(*p), ==, 2);

	pool_remove(*p, 0);
	g_assert_cmpuint(pool_len(*p), ==, 1);

	pool_remove(*p, 1);
	g_assert_cmpuint(pool_len(*p), ==, 0);
}

static void
test_iter(Pool **p, gconstpointer user_data)
{
	char *data = " e lo";
	for (int i = 0; i < strlen(data); i++) {
		if (data[i] == ' ')
			continue;
		pool_set(*p, i, data + i);
	}

	PoolIter i;
	pool_iter_init(*p, &i);
	while (pool_iter_next(&i)) {
		char *ch = pool_iter_get(&i);
		g_assert_cmpuint(*ch, ==, data[pool_iter_index(&i)]);
	}
}

static void
test_iter_modified(Pool **p, gconstpointer user_data)
{
	//            01234567
	char *data = "abcdefgh";
	for (int i = 0; i < strlen(data); i++)
		pool_add(*p, data + i);

	pool_remove(*p, 0);
	pool_remove(*p, 2);
	pool_remove(*p, 3);
	pool_remove(*p, 7);

	int offset = 0;
	char data_copy[8] = {0};

	PoolIter i;
	pool_iter_init(*p, &i);
	while (pool_iter_next(&i)) {
		data_copy[offset++] = *(char*)pool_iter_get(&i);
	}

	g_assert_cmpstr(data_copy, ==, "befg");
}

void
pool_suite_setup()
{
	g_test_add("/pool/create", Pool*, NULL, setup, test_create, teardown);
	g_test_add("/pool/reserve", Pool*, NULL, setup, test_reserve, teardown);
	g_test_add("/pool/use", Pool*, NULL, setup, test_use, teardown);
	g_test_add("/pool/remove", Pool*, NULL, setup, test_remove, teardown);
	g_test_add("/pool/iter", Pool*, NULL, setup, test_iter, teardown);
	g_test_add("/pool/iter_modified", Pool*, NULL, setup, test_iter_modified, teardown);
}
