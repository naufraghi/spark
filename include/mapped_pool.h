#pragma once

#include "pool.h"
#include <stdbool.h>
#include <stdint.h>

typedef struct MappedPool MappedPool;

MappedPool*
mapped_pool_new(size_t element_size, int count, int grow_by);

Pool*
mapped_pool_storage_get(MappedPool *mp);

bool
mapped_pool_set(MappedPool *mp, uint32_t key, void *value);

void*
mapped_pool_get(MappedPool *mp, uint32_t key);

bool
mapped_pool_pop(MappedPool *mp, uint32_t key);

void
mapped_pool_free(MappedPool *mp);
