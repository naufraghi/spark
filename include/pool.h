#pragma once

#include <stdbool.h>
#include <stddef.h>

typedef struct Pool Pool;

typedef struct {
	Pool *p;
	size_t i;
} PoolIter;

Pool*
pool_new(size_t element_size, int count, int grow_by);

void
pool_free(Pool *p);

size_t
pool_len(Pool *p);

size_t
pool_size(Pool *p);

size_t
pool_grow_size(Pool *p);

int
pool_reserve(Pool *p);

void
pool_set(Pool *p, int index, const void *data);

int
pool_add(Pool *p, const void *data);

void*
pool_get(Pool *p, int index);

void
pool_remove(Pool *p, int index);

void
pool_iter_init(Pool *p, PoolIter *i);

bool
pool_iter_next(PoolIter *iter);

void*
pool_iter_get(PoolIter *iter);

int
pool_iter_index(PoolIter *iter);
