#pragma once

#include "defines.h"
#include "pool.h"
#include <glib.h>

struct Game {
	GHashTable *systems;
	GArray *reg_order;
	Pool *event_queue;
	Pool *entities;
	int log_level;
	GHashTable *subscriptions;
};

struct EventSubscription {
	EventHandler handler;
	void *data;
};

struct EntityInfo {
	Entity entity;
	GHashTable *components;
};

void
dispatch_events();

void
unsubscribe_entity(Entity e);

void
unsubscribe_entity_component(Entity e, ComponentType t);
