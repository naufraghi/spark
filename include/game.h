#pragma once

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#define COMPONENT_ANY  0
#define GAME_CORE      1

#define ENTITY_ANY     0
#define ROOT_ENTITY    1

enum {
	EVENT_ANY,
	EVENT_ENTITY_CREATED,
	EVENT_ENTITY_DESTROYED,
};


/**
 * Entity identifier type.
 *
 * Entities are just plain numbers which identify uniquely a set of components,
 * acting basically as "containers".
 *
 * Note: storing entity values is fine, as long as the entity exists.
 */
typedef uint16_t Entity;


/**
 * Component type.
 *
 * Numeric value which identifies a component type; since component types are
 * global, values must be distinct.
 */
typedef uint8_t ComponentType;


/**
 * Event type.
 *
 * Identifies a particular type of event within a component system.
 */
typedef uint8_t EventType;


/**
 * Log levels.
 */
enum {
	LOG_DISABLED = 0,
	LOG_DEBUG,
	LOG_INFO,
	LOG_WARN,
	LOG_ERROR,
	LOG_FATAL
};


/**
 * Game event.
 *
 * Contains information about a game event: the component type of the system it
 * comes from, the particular event type and the source entity.
 *
 * Game engine events have GAME_CORE component type.
 */
struct Event {
	// type of component which fired the event
	ComponentType component;
	// event type
	EventType event;
	// identifier of the entity which owns the component
	Entity entity;
	// event data; specific to event type
	void *data;
	// event data size; read-only
	size_t data_size;
};


/**
 * Event handler prototype.
 */
typedef void (*EventHandler)(struct Event *evt, void *data);


/**
 * Component system.
 *
 * Component systems act as component factories and incapsulate the creation,
 * initialization and logic for a specific type of component.
 *
 * After a system has been registered for a given component type, entities with
 * components of that type can be created.
 */
struct System {
	// system name; read-only
	char *name;
	// user data
	void *data;

	// system initialization method
	void (*init)(struct System *self);

	// system shutdown method
	void (*exit)(struct System *self);

	// system update method
	void (*update)(struct System *self);

	// component creation method; this must provide a component for given
	// entity
	void (*create_component)(struct System *self, Entity entity);

	// component destruction method; the passed entity is guaranteed to have
	// the component
	void (*destroy_component)(struct System *self, Entity entity);

	// component data accessor method; the passed entity is guaranteed to
	// have the component
	void* (*get_component)(struct System *self, Entity entity);
};

/**
 * Initialize the engine.
 */
void
game_init();

/**
 * Shutdown the engine.
 */
void
game_exit();

/**
 * Update game.
 *
 * Calls update() for all systems which provide one, following the registration
 * order, and dispatches the events queued from last call to subscribers.
 */
void
game_update();

/**
 * Register a component system.
 *
 */
void
game_system_register(ComponentType t, struct System *s);

/**
 * Unregister a component system.
 *
 * Shuts down and unregisters the system bound to the given component type.
 */
void
game_system_unregister(ComponentType t);

/**
 * Retrieve the component system.
 *
 * Returns the component system for the specified component type.
 */
struct System*
game_system_get(ComponentType t);

/**
 * Create a game entity.
 *
 * Creates a game entity with given components, specified as 0-terminated array
 * of ComponentType values.
 */
Entity
game_entity_create(const ComponentType *components);

/**
 * Destroy an entity.
 *
 * Destroys the specified entity and all components referenced by it.
 * This also removes all entity-specific event subscribers.
 */
void
game_entity_destroy(Entity entity);

/**
 * Add a component to an entity.
 *
 * Adds a component to given entity. Same type components cannot be added twice.
 */
void
game_entity_component_add(Entity entity, ComponentType component);

/**
 * Get the component data.
 *
 * Returns a void pointer to given component data.
 */
void*
game_entity_component_get(Entity entity, ComponentType component);

/**
 * Remove a component from entity.
 *
 * Destroys the specified component for the given entity and unsubscribes
 * immediately all event listeners for this component type.
 *
 * Does nothing if the entity does not have the component.
 */
void
game_entity_component_remove(Entity entity, ComponentType t);

/**
 * Check if the given entity has the specified component.
 */
bool
game_entity_has_component(Entity entity, ComponentType t);

/**
 * Subscribe to an event.
 *
 * Creates a subscription for the specified component type, event and entity
 * combination. When the event occurs, the provided callback will be called.
 *
 * Optional user data pointer related to the specific subscription can be
 * passed, which will be then passed back to the handler.
 */
void
game_event_subscribe(
	ComponentType component,
	EventType event,
	Entity entity,
	EventHandler handler,
	void *data
);

/**
 * Unsubsribe a callback for given event.
 */
void
game_event_unsubscribe(
	ComponentType component,
	EventType event,
	Entity entity,
	EventHandler handler
);

/**
 * Unsubscribe all registered callbacks for given event.
 */
void
game_event_unsubscribe_all(
	ComponentType component,
	EventType event,
	Entity entity
);

/**
 * Push an event.
 *
 * Creates and pushes an Event structure into the engine's event queue, which
 * will be dispatched at next game_update() call.
 *
 * An optional user data, which normally contains additional event information,
 * can be stored in the event. In case data_size is not zero, the user data will
 * be copied and then automatically freed after the event has been dispatched to
 * all subscribers.
 *
 */
void
game_event_push(
	ComponentType component,
	EventType event,
	Entity entity,
	void *data,
	size_t data_size
);

/**
 * Log a message.
 */
void
game_log(int level, const char *fmt, ...);

/**
 * Set the log verbosity level.
 */
void
game_log_level_set(int level);
