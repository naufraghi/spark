CFLAGS:= $(CFLAGS) -std=gnu99 -ggdb -fvisibility=hidden \
         $(shell pkg-config --cflags glib-2.0) \
         -I$(PWD)/include

LDFLAGS:= $(LDFLAGS) $(LIBS) \
	  $(shell pkg-config --libs glib-2.0)

export CFLAGS
export LDFLAGS
export TEST_CFLAGS=$(CFLAGS)
export TEST_LDFLAGS=$(LDFLAGS) -L$(PWD)/src -Wl,-rpath,$(PWD)/src -lgame

all:
	$(MAKE) -C src/

clean:
	$(MAKE) -C src/ clean
	$(MAKE) -C tests/ clean

test:
	$(MAKE) -C src/
	$(MAKE) -C tests/
