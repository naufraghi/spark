#include "defines.h"
#include "pool.h"
#include <glib.h>
#include <stdbool.h>
#include <string.h>

struct Pool {
	bool *usage_mask;
	void *data;
	size_t element_size;
	int size;
	int grow_by;
};

EXPORT Pool*
pool_new(size_t element_size, int size, int grow_by)
{
	g_assert(size > 0);
	g_assert(grow_by > 0);

	Pool *p = g_new(Pool, 1);
	p->usage_mask = g_new0(bool, size);
	p->data = g_malloc(element_size * size);
	p->size = size;
	p->grow_by = grow_by;
	p->element_size = element_size;

	return p;
}

EXPORT void
pool_free(Pool *p)
{
	g_assert_nonnull(p);
	g_free(p->usage_mask);
	g_free(p->data);
	g_free(p);
}

EXPORT size_t
pool_size(Pool *p)
{
	return p->size;
}

EXPORT size_t
pool_grow_size(Pool *p)
{
	return p->grow_by;
}

EXPORT size_t
pool_len(Pool *p)
{
	size_t len = 0;
	for (int i = 0; i < p->size; i++) {
		if (p->usage_mask[i])
			len++;
	}
	return len;
}

EXPORT int
pool_reserve(Pool *p)
{
	g_assert_nonnull(p);

	int idx = -1;
	for (int i = 0; i < p->size; i++) {
		if (p->usage_mask[i] == false) {
			idx = i;
			break;
		}
	}

	if (idx == -1) {
		p->usage_mask = g_renew(
			bool, p->usage_mask, p->size + p->grow_by
		);
		memset(p->usage_mask + sizeof(bool) * p->size, 0, p->grow_by);

		p->data = g_realloc(
			p->data, p->element_size * (p->size + p->grow_by)
		);
		memset(p->data + sizeof(bool) * p->size, 0, p->grow_by);

		idx = p->size;
		p->size += p->grow_by;
	}
	p->usage_mask[idx] = true;
	return idx;
}


EXPORT void
pool_set(Pool *p, int index, const void *data)
{
	g_assert_nonnull(p);
	g_assert_nonnull(data);
	g_assert(index >= 0);
	g_assert(index < p->size);

	p->usage_mask[index] = true;
	memcpy(p->data + p->element_size * index, data, p->element_size);
}

EXPORT int
pool_add(Pool *p, const void *data)
{
	g_assert_nonnull(p);
	g_assert_nonnull(data);

	int idx = pool_reserve(p);
	pool_set(p, idx, data);

	return idx;
}

EXPORT void*
pool_get(Pool *p, int index)
{
	g_assert_nonnull(p);
	g_assert(index >= 0);
	g_assert(index < p->size);

	if (p->usage_mask[index])
		return p->data + p->element_size * index;
	return NULL;
}

EXPORT void
pool_remove(Pool *p, int index)
{
	g_assert_nonnull(p);
	g_assert(index >= 0);
	g_assert(index < p->size);

	p->usage_mask[index] = false;
}


EXPORT void
pool_iter_init(Pool *p, PoolIter *iter)
{
	g_assert_nonnull(p);
	iter->i = 0;
	iter->p = p;
}

EXPORT bool
pool_iter_next(PoolIter *iter)
{
	g_assert_nonnull(iter);
	while (iter->i < pool_size(iter->p)) {
		if (iter->p->usage_mask[iter->i++])
			return true;
	}
	return false;
}

EXPORT void*
pool_iter_get(PoolIter *iter)
{
	g_assert_nonnull(iter);
	g_assert_true(iter->i >= 0);
	return pool_get(iter->p, iter->i - 1);
}

EXPORT int
pool_iter_index(PoolIter *iter)
{
	g_assert_nonnull(iter);
	g_assert_true(iter->i >= 0);
	return iter->i - 1;
}
