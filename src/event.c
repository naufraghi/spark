#include "game.h"
#include "internals.h"
#include <glib.h>
#include <stdbool.h>

extern struct Game game;

/**
 * Create a packed integer identifier suitable as subscription key.
 */
static uint32_t
make_key(ComponentType component, EventType event, Entity entity)
{
	uint32_t s = entity;
	s |= event << 16;
	s |= component << 24;
	return s;
}


/**
 * Get the existing GArray for given key, or create and insert a new one if none
 * exists.
 */
static GArray*
ht_get_default_array(GHashTable *ht, uint32_t key)
{
	GArray *array = g_hash_table_lookup(ht, GUINT_TO_POINTER(key));
	if (!array) {
		array = g_array_new(false, false, sizeof(struct EventSubscription));
		g_hash_table_insert(ht, GUINT_TO_POINTER(key), array);
	}

	return array;
}


/**
 * Dispatch the given event to all subscriptions bound to the specified signature.
 */
void
dispatch_event(struct Event *evt, uint32_t key)
{
	GArray *subscriptions = g_hash_table_lookup(
		game.subscriptions, GUINT_TO_POINTER(key)
	);
	if (!subscriptions)
		return;

	for (int s = 0; s < subscriptions->len; s++) {
		struct EventSubscription *es = &g_array_index(
			subscriptions, struct EventSubscription, s
		);
		es->handler(evt, es->data);
	}
}


/**
 * Perform event dispatching.
 */
void
dispatch_events()
{
	PoolIter i;
	pool_iter_init(game.event_queue, &i);

	while (pool_iter_next(&i)) {
		struct Event *evt = pool_iter_get(&i);
		uint32_t key;

		// dispatch to exact event subscriptions
		key = make_key(evt->component, evt->event, evt->entity);
		dispatch_event(evt, key);

		// dispatch to entity-agnostic subscriptions
		key = make_key(evt->component, evt->event, ENTITY_ANY);
		dispatch_event(evt, key);

		// dispatch to event-agnostic subscriptions
		key = make_key(evt->component, EVENT_ANY, evt->entity);
		dispatch_event(evt, key);

		// dispatch to component-wide subscriptions
		key = make_key(evt->component, EVENT_ANY, ENTITY_ANY);
		dispatch_event(evt, key);

		// dispatch to the most generic possible subscriptions
		key = make_key(COMPONENT_ANY, EVENT_ANY, ENTITY_ANY);
		dispatch_event(evt, key);

		// free copied data
		if (evt->data_size)
			g_free(evt->data);

		pool_remove(game.event_queue, pool_iter_index(&i));
	}
}

void
remove_subscription(uint32_t key, EventHandler hnd)
{
	GArray *subscriptions = g_hash_table_lookup(
		game.subscriptions, GUINT_TO_POINTER(key)
	);

	if (subscriptions) {
		for (int i = 0; i < subscriptions->len; i++) {
			struct EventSubscription *s = &g_array_index(
				subscriptions,
				struct EventSubscription,
				i
			);

			if (s->handler == hnd) {
				g_array_remove_index(subscriptions, i);
				--i;
			}
		}
	}
}

void
remove_subscriptions(uint32_t key)
{
	GArray *subscriptions = g_hash_table_lookup(
		game.subscriptions, GUINT_TO_POINTER(key)
	);

	if (subscriptions) {
		g_array_free(subscriptions, true);
		g_hash_table_remove(game.subscriptions, GUINT_TO_POINTER(key));
	}
}

/**
 * Unsubsribe all handlers for given entity.
 */
void
unsubscribe_entity(Entity e)
{
	struct EntityInfo *einfo = pool_get(game.entities, e - 1);
	GHashTableIter i;
	gpointer k, v;

	g_hash_table_iter_init(&i, einfo->components);
	while (g_hash_table_iter_next(&i, &k, &v)) {
		unsigned int component = GPOINTER_TO_UINT(k);
		uint32_t key;

		key = make_key(component, 0, e);
		remove_subscriptions(key);

		// FIXME: implement something more efficient
		for (int event = 0; event < 255; event++) {
			key = make_key(component, event, e);
			remove_subscriptions(key);
		}
	}
}

/**
 * Unsubscribe all handlers for given component
 */
void
unsubscribe_entity_component(Entity e, ComponentType t)
{
	remove_subscriptions(make_key(t, 0, e));

	// FIXME: implement something more efficient
	for (int event = 0; event < 255; event++) {
		remove_subscriptions(make_key(t, event, e));
	}
}


EXPORT void
game_event_subscribe(
	ComponentType component,
	EventType event,
	Entity entity,
	EventHandler handler,
	void *data
) {
	uint32_t key = make_key(component, event, entity);
	struct EventSubscription sub = {
		.handler = handler,
		.data = data
	};

	GArray *subscriptions = ht_get_default_array(game.subscriptions, key);
	g_array_append_val(subscriptions, sub);
}


EXPORT void
game_event_push(
	ComponentType component,
	EventType event,
	Entity entity,
	void *data,
	size_t data_size
) {
	g_assert_true(component != 0);
	g_assert_true(event != 0);
	g_assert_true(entity != 0);

	struct Event e = {
		.component = component,
		.event = event,
		.entity = entity,
		.data = data_size ? g_memdup(data, data_size) : data,
		.data_size = data_size
	};
	pool_add(game.event_queue, &e);
}

EXPORT void
game_event_unsubscribe(
	ComponentType component,
	EventType event,
	Entity entity,
	EventHandler handler
) {
	uint32_t key = make_key(component, event, entity);
	remove_subscription(key, handler);
}


EXPORT void
game_event_unsubscribe_all(
	ComponentType component,
	EventType event,
	Entity entity
) {
	uint32_t key = make_key(component, event, entity);
	remove_subscriptions(key);
}
