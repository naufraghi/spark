#include "defines.h"
#include "mapped_pool.h"
#include <glib.h>

struct MappedPool {
	Pool *storage;
	GHashTable *index_map;
};

EXPORT MappedPool*
mapped_pool_new(size_t element_size, int count, int grow_by)
{
	MappedPool *mp = g_new0(MappedPool, 1);
	mp->storage = pool_new(element_size, count, grow_by);
	mp->index_map = g_hash_table_new(NULL, NULL);

	return mp;
}

EXPORT Pool*
mapped_pool_storage_get(MappedPool *mp)
{
	return mp->storage;
}

EXPORT bool
mapped_pool_set(MappedPool *mp, uint32_t key, void *value)
{
	bool new = false;
	uint32_t idx = GPOINTER_TO_UINT(g_hash_table_lookup(
		mp->index_map, GUINT_TO_POINTER(key)
	));

	if (idx == 0) {
		new = !g_hash_table_contains(
			mp->index_map, GUINT_TO_POINTER(key)
		);

		if (new) {
			idx = pool_reserve(mp->storage);
			g_hash_table_insert(
				mp->index_map,
				GUINT_TO_POINTER(key),
				GUINT_TO_POINTER(idx)
			);
		}
	}

	pool_set(mp->storage, idx, value);
	return new;
}

EXPORT bool
mapped_pool_pop(MappedPool *mp, uint32_t key)
{
	if (g_hash_table_contains(mp->index_map, GUINT_TO_POINTER(key))) {
		uint32_t idx = GPOINTER_TO_INT(g_hash_table_lookup(
			mp->index_map, GUINT_TO_POINTER(key)
		));

		pool_remove(mp->storage, idx);

		g_hash_table_remove(mp->index_map, GUINT_TO_POINTER(key));

		return true;
	}

	return false;
}

EXPORT void*
mapped_pool_get(MappedPool *mp, uint32_t key)
{
	uint32_t idx = GPOINTER_TO_UINT(g_hash_table_lookup(
		mp->index_map, GUINT_TO_POINTER(key)
	));

	if (idx == 0) {
		// double-check the 0 index key
		bool exists = g_hash_table_contains(
			mp->index_map, GUINT_TO_POINTER(key)
		);

		if (!exists) {
			return NULL;
		}
	}

	return pool_get(mp->storage, idx);
}

EXPORT void
mapped_pool_free(MappedPool *mp)
{
	pool_free(mp->storage);
	g_hash_table_destroy(mp->index_map);
	g_free(mp);
}
