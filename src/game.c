#include "game.h"
#include "internals.h"
#include "pool.h"
#include <glib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef GAME_LOG_LEVEL
#define GAME_LOG_LEVEL LOG_ERROR
#endif

#define EVENTS_BUFFER_SIZE 100

#define ENTITIES_POOL_SIZE 1000

#define assert_entity_is_valid(__entity) g_assert(__entity != 0);

struct Game game;

EXPORT void
game_init()
{
	game.systems = g_hash_table_new(NULL, NULL);
	game.subscriptions = g_hash_table_new(NULL, NULL);
	game.reg_order = g_array_new(false, false, sizeof(ComponentType));
	game.event_queue = pool_new(sizeof(struct Event), 10, 10);
	game.log_level = GAME_LOG_LEVEL;

	game.entities = pool_new(
		sizeof(struct EntityInfo),
		ENTITIES_POOL_SIZE,
		ENTITIES_POOL_SIZE / 10
	);

	struct System *core_sys = g_new0(struct System, 1);
	core_sys->name = "game";
	game_system_register(GAME_CORE, core_sys);

	// create the root entity with no components
	game_entity_create(NULL);
}

EXPORT void
game_exit()
{
	// destroy entities
	PoolIter i;
	pool_iter_init(game.entities, &i);
	while (pool_iter_next(&i)) {
		struct EntityInfo *ei = pool_iter_get(&i);
		game_entity_destroy(ei->entity);
	}
	pool_free(game.entities);

	// TODO: deep deallocation
	// g_hash_table_destroy(game.subscriptions);

	// shutdown and free systems
	for (int i = game.reg_order->len - 1; i >= 0; i--) {
		ComponentType t = g_array_index(
			game.reg_order, ComponentType, i
		);
		struct System *sys = g_hash_table_lookup(
			game.systems, GINT_TO_POINTER(t)
		);

		if (sys->exit) {
			sys->exit(sys);
		}

		game_log(
			LOG_DEBUG,
			"shut down '%s' system",
			sys->name
		);
		free(sys);
	}
	g_hash_table_destroy(game.systems);

	g_array_free(game.reg_order, true);
	pool_free(game.event_queue);
}

EXPORT void
game_update()
{
	for (int i = 0; i < game.reg_order->len; i++) {
		ComponentType t = g_array_index(
			game.reg_order, ComponentType, i
		);
		struct System *sys = g_hash_table_lookup(
			game.systems, GINT_TO_POINTER(t)
		);

		if (sys->update)
			sys->update(sys);

		dispatch_events();
	}
}

EXPORT void
game_system_register(ComponentType t, struct System *s)
{
	g_assert_nonnull(s);
	g_assert_nonnull(s->name);

	// the first system being registered is always the core one, others must
	// not compete for component type GAME_CORE
	if (g_hash_table_size(game.systems) != 0)
		g_assert_true(t > GAME_CORE);

	g_hash_table_insert(game.systems, GINT_TO_POINTER(t), s);
	g_array_append_val(game.reg_order, t);

	game_log(LOG_DEBUG, "initialized '%s' system", s->name);

	if (s->init) {
		s->init(s);
	}
};

EXPORT void
game_system_unregister(ComponentType t)
{
	g_assert_true(t > GAME_CORE);

	struct System *s = g_hash_table_lookup(
		game.systems, GINT_TO_POINTER(t)
	);

	if (s) {
		// remove system components from all entities
		PoolIter i;
		pool_iter_init(game.entities, &i);
		while (pool_iter_next(&i)) {
			struct EntityInfo *ei = pool_iter_get(&i);
			game_entity_component_remove(ei->entity, t);
		}

		// shutdown system and remove it from systems hash table
		if (s->exit)
			s->exit(s);
		g_hash_table_remove(game.systems, GINT_TO_POINTER(t));

		// remove system also from the registered systems array
		for (int i = 0; i < game.reg_order->len; i++) {
			ComponentType ct = g_array_index(
				game.reg_order, ComponentType, i
			);

			if (ct == t) {
				g_array_remove_index(game.reg_order, i);
				break;
			}
		}

		g_free(s);
	}
}

EXPORT struct System*
game_system_get(ComponentType t)
{
	g_assert_true(t > GAME_CORE);
	return g_hash_table_lookup(game.systems, GINT_TO_POINTER(t));
}

EXPORT Entity
game_entity_create(const ComponentType *components)
{
	Entity entity = pool_reserve(game.entities) + 1;
	struct EntityInfo *einfo = pool_get(game.entities, entity - 1);
	einfo->entity = entity;
	einfo->components = g_hash_table_new(NULL, NULL);

	game_log(LOG_DEBUG, "created entity %d", entity);

	if (components) {
		while (*components) {
			game_entity_component_add(entity, *components);
			components++;
		}
	}

	game_event_push(GAME_CORE, EVENT_ENTITY_CREATED, entity, NULL, 0);

	return entity;
}

EXPORT void
game_entity_destroy(Entity entity)
{
	assert_entity_is_valid(entity);
	struct EntityInfo *einfo = pool_get(game.entities, entity - 1);
	if (!einfo)
		game_log(LOG_FATAL, "entity %d does not exist", entity);

	unsubscribe_entity(entity);

	GHashTableIter iter;
	gpointer key, value;

	g_hash_table_iter_init(&iter, einfo->components);
	while (g_hash_table_iter_next(&iter, &key, &value)) {
		struct System *sys = g_hash_table_lookup(game.systems, key);
		if (sys->destroy_component) {
			sys->destroy_component(sys, einfo->entity);
			game_log(
				LOG_DEBUG,
				"destroyed component '%s' of entity %d",
				sys->name,
				einfo->entity
			);
		}
	}
	g_hash_table_destroy(einfo->components);

	pool_remove(game.entities, entity - 1);

	game_event_push(GAME_CORE, EVENT_ENTITY_DESTROYED, entity, NULL, 0);
	game_log(
		LOG_DEBUG,
		"destroyed entity %d",
		entity
	);
}


EXPORT void
game_entity_component_add(Entity entity, ComponentType t)
{
	g_assert_true(t > GAME_CORE);
	assert_entity_is_valid(entity);

	// get the components set for given entity
	struct EntityInfo *einfo = pool_get(game.entities, entity - 1);
	if (!einfo)
		game_log(LOG_FATAL, "entity %d does not exist", entity);

	// ensure the component is not already possessed
	if (g_hash_table_contains(einfo->components, GINT_TO_POINTER(t))) {
		game_log(
			LOG_FATAL,
			"entity %d already has the component %d",
			entity,
			t
		);
	}

	// lookup the component system and attempt to create a new component
	struct System *sys = g_hash_table_lookup(
		game.systems, GINT_TO_POINTER(t)
	);
	if (sys) {
		if (sys->create_component) {
			sys->create_component(sys, entity);
			g_hash_table_add(einfo->components, GINT_TO_POINTER(t));
			game_log(
				LOG_DEBUG,
				"added component '%s' to entity %d",
				sys->name,
				entity
			);
		}
		else {
			game_log(
				LOG_FATAL,
				"system '%s' does not allow component "
				"instantiation",
				sys->name
			);
		}
	}
	else {
		game_log(LOG_FATAL, "unknown component %d",t);
	}
}


EXPORT void*
game_entity_component_get(Entity entity, ComponentType t)
{
	g_assert_true(t > GAME_CORE);
	assert_entity_is_valid(entity);

	if (!pool_get(game.entities, entity - 1))
		game_log(LOG_FATAL, "entity %d does not exist", entity);

	// lookup the system and retrieve a pointer to component data
	struct System *sys = g_hash_table_lookup(
		game.systems, GINT_TO_POINTER(t)
	);
	if (sys) {
		if (sys->get_component) {
			void *c = sys->get_component(sys, entity);
			if (!c) {
				game_log(
					LOG_FATAL,
					"entity %d does not have component '%s'",
					entity,
					sys->name
				);
			}
			return c;
		}
		else {
			game_log(
				LOG_FATAL,
				"system '%s' does not allow component retrieval",
				sys->name
			);
		}
	}
	else {
		game_log(LOG_FATAL, "unknown component %d", t);
	}
	return NULL;
}

EXPORT void
game_entity_component_remove(Entity entity, ComponentType t)
{
	g_assert_true(t > GAME_CORE);
	assert_entity_is_valid(entity);

	if (!pool_get(game.entities, entity - 1))
		game_log(LOG_FATAL, "entity %d does not exist", entity);

	gpointer key = GUINT_TO_POINTER(t);

	struct EntityInfo *einfo = pool_get(game.entities, entity - 1);
	if (!einfo)
		game_log(LOG_FATAL, "entity %d does not exist", entity);

	if (g_hash_table_contains(einfo->components, key)) {
		struct System *sys = g_hash_table_lookup(game.systems, key);
		if (sys->destroy_component)
			sys->destroy_component(sys, entity);
		g_hash_table_remove(einfo->components, key);
		unsubscribe_entity_component(entity, t);
	}
}

EXPORT bool
game_entity_has_component(Entity entity, ComponentType t)
{
	g_assert_true(t > GAME_CORE);
	assert_entity_is_valid(entity);

	gpointer key = GUINT_TO_POINTER(t);

	struct EntityInfo *einfo = pool_get(game.entities, entity - 1);
	if (!einfo)
		game_log(LOG_FATAL, "entity %d does not exist", entity);

	return g_hash_table_contains(einfo->components, key);
}

EXPORT void
game_log(int level, const char *fmt, ...)
{
	g_assert_nonnull(fmt);

	va_list ap;
	va_start(ap, fmt);

	if (level < game.log_level)
		return;

	switch (level) {
	case LOG_DEBUG:
	case LOG_INFO:
	case LOG_WARN:
		vprintf(fmt, ap);
		printf("\n");
		break;

	case LOG_ERROR:
	case LOG_FATAL:
		vfprintf(stderr, fmt, ap);
		fprintf(stderr, "\n");
		if (level == LOG_FATAL)
			abort();
		break;

	default:
		fprintf(stderr, "unknown log level\n");
		exit(EXIT_FAILURE);
	}
}

EXPORT void
game_log_level_set(int level)
{
	g_assert_true(level >= 0 && level <= LOG_FATAL);
	game.log_level = level;
}
